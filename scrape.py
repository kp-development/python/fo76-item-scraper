#! /usr/bin/env python3

_ammo = "https://fallout.fandom.com/wiki/Fallout_76_ammunition"
_armor = "https://fallout.fandom.com/wiki/Fallout_76_armor_and_clothing"
_junk = "https://fallout.fandom.com/wiki/Fallout_76_junk_items"
_weapons = "https://fallout.fandom.com/wiki/Fallout_76_weapons"
_consumables = "https://fallout.fandom.com/wiki/Fallout_76_consumables"
_plans = "https://fallout.fandom.com/wiki/Fallout_76_plans"
_recipes = "https://fallout.fandom.com/wiki/Fallout_76_recipes"
_misc = "https://fallout.fandom.com/wiki/Fallout_76_miscellaneous_items"

# fire us up
from work.runit import KP_FO76_RunIT
_crawler = KP_FO76_RunIT( )

# pull miscelaneous items
print("MISCELANEOUS")
_crawler.PullMisc( _misc )

# pull recipes
print("RECIPES")
_crawler.PullRecipes( _recipes )

# pull plans
print("PLANS")
_crawler.PullPlans( _plans )

# pull consumables
print("CONSUMABLES")
_crawler.PullConsumables( _consumables )

# pull weapons
print("WEAPONS")
_crawler.PullWeapons( _weapons )

# pull junk


# pull armor


# pull ammo


del _crawler