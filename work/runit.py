#! /usr/bin/env python3

# some necessary imports
import requests, getpass, json, operator, mimetypes
from os.path import splitext
from urllib.parse import urlparse, urljoin
from urllib3.exceptions import InsecureRequestWarning
from bs4 import BeautifulSoup

class KP_FO76_RunIT:

    # fire up our class and setup some inherent variables
    def __init__( self ):

         # setup our requests header
        _headers = { 'User-Agent': 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:17.0) Gecko/20100101 Firefox/17.0' }

        # supress the cert warnings
        requests.packages.urllib3.disable_warnings( category=InsecureRequestWarning )

        # setup out requesting session
        self._session = requests.Session( )

        # add the User-Agent header
        self._session.headers.update( _headers )

        # base url
        self.base_url = "https://fallout.fandom.com"


    # clean up
    def __del__( self ):

        # close our session first
        self._session.close

        # clean up
        del self._session

    # pull weapons
    def PullWeapons( self, _uri ):

        # get a response from our request
        _resp = self._session.get( _uri, allow_redirects=True, verify=False )

        # make sure we actually have an OK response before proceeding
        if _resp.ok:

            # process the miscelaneous item data
            from work.inc.weapons import KP_FO76_Weapons
            _weapons = KP_FO76_Weapons( )

            # the list data, we will need this for the details
            _list = _weapons.ListData( _resp, self.base_url )
            print(_list)

    # pull consumables
    def PullConsumables( self, _uri ):

        # get a response from our request
        _resp = self._session.get( _uri, allow_redirects=True, verify=False )

        # make sure we actually have an OK response before proceeding
        if _resp.ok:

            # process the miscelaneous item data
            from work.inc.consumables import KP_FO76_Consumables
            _consumables = KP_FO76_Consumables( )

            # the list data, we will need this for the details
            _list = _consumables.ListData( _resp, self.base_url )
            print(_list)

    # pull plans
    def PullPlans( self, _uri ):

        # get a response from our request
        _resp = self._session.get( _uri, allow_redirects=True, verify=False )

        # make sure we actually have an OK response before proceeding
        if _resp.ok:

            # process the miscelaneous item data
            from work.inc.plans import KP_FO76_Plans
            _plans = KP_FO76_Plans( )

            # the list data, we will need this for the details
            _list = _plans.ListData( _resp, self.base_url )
            print(_list)


    #pull recipes
    def PullRecipes( self, _uri ):

        # get a response from our request
        _resp = self._session.get( _uri, allow_redirects=True, verify=False )

        # make sure we actually have an OK response before proceeding
        if _resp.ok:

            # process the miscelaneous item data
            from work.inc.recipes import KP_FO76_Recipes
            _recipes = KP_FO76_Recipes( )

            # the list data, we will need this for the details
            _list = _recipes.ListData( _resp, self.base_url )
            print(_list)

    # pull the misc items
    def PullMisc( self, _uri ):

        # get a response from our request
        _resp = self._session.get( _uri, allow_redirects=True, verify=False )

        # make sure we actually have an OK response before proceeding
        if _resp.ok:

            # process the miscelaneous item data
            from work.inc.misc import KP_FO76_Misc
            _misc = KP_FO76_Misc( )

            # the list data, we will need this for the details
            _list = _misc.ListData( _resp, self.base_url )
            print(_list)