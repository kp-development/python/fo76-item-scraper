#! /usr/bin/env python3

from bs4 import BeautifulSoup

class KP_FO76_Consumables:

    def ListData( self, _resp, _uri ):

        # all items`
        _items = []

        # fire up the soup to parse the content
        _soup = BeautifulSoup( _resp.text, 'lxml' )

        # get our datatables
        _tables = _soup.findAll( 'table', {'class': 'va-table'} )

        # FOOD
        # the tables are sructured differently, so we'll need to parse each separately
        _rows = _tables[0].findAll( 'tr' )

        # loop the rows
        for row in _rows:

            # get our columns
            cols = row.findAll( 'td' )

            if cols:

                # get a link to the items details page
                _link = cols[1].find( 'a' ).attrs.get( "href" )

                # now we can strip everything out from it row, and just return it's textual content
                cols=[x.text.strip() for x in cols]
                _name = cols[1]
                _value = cols[8]
                _weight = cols[7]

                # add in the extra data
                _extra = []
                _extra.append( {
                    'rads' : cols[2], 
                    'hp' : cols[3], 
                    'effect' : cols[4], 
                    'disease' : cols[5], 
                    'food' : cols[6], 
                    'addiction' : cols[9], 
                    'plantable' : cols[11]
                } )

                _items.append(
                    {
                        'name': _name,
                        'image': None,
                        'link': "{}{}".format( _uri, _link ),
                        'value': _value,
                        'weight': _weight,
                        'related_quests': None,
                        'extra': _extra
                    }
                )

        # SOUPS
        # the tables are sructured differently, so we'll need to parse each separately
        _rows = _tables[1].findAll( 'tr' )

        # loop the rows
        for row in _rows:

            # get our columns
            cols = row.findAll( 'td' )

            if cols:

                # get a link to the items details page
                _link = cols[1].find( 'a' ).attrs.get( "href" )

                # now we can strip everything out from it row, and just return it's textual content
                cols=[x.text.strip() for x in cols]
                _name = cols[1]
                _value = cols[9]
                _weight = cols[8]

                # add in the extra data
                _extra = []
                _extra.append( {
                    'rads' : cols[2], 
                    'hp' : cols[3], 
                    'effect' : cols[4], 
                    'disease' : cols[5], 
                    'food' : cols[6], 
                    'water' : cols[7]
                } )

                _items.append(
                    {
                        'name': _name,
                        'image': None,
                        'link': "{}{}".format( _uri, _link ),
                        'value': _value,
                        'weight': _weight,
                        'related_quests': None,
                        'extra': _extra
                    }
                )

        # DRINKS
        # the tables are sructured differently, so we'll need to parse each separately
        _rows = _tables[2].findAll( 'tr' )

        # loop the rows
        for row in _rows:

            # get our columns
            cols = row.findAll( 'td' )

            if cols:

                # get a link to the items details page
                _link = cols[1].find( 'a' ).attrs.get( "href" )

                # now we can strip everything out from it row, and just return it's textual content
                cols=[x.text.strip() for x in cols]
                _name = cols[1]
                _value = cols[9]
                _weight = cols[8]

                # add in the extra data
                _extra = []
                _extra.append( {
                    'rads' : cols[2], 
                    'hp' : cols[3], 
                    'ap' : cols[4], 
                    'effect' : cols[5], 
                    'disease' : cols[6], 
                    'water' : cols[7], 
                    'addiction' : cols[10]
                } )

                _items.append(
                    {
                        'name': _name,
                        'image': None,
                        'link': "{}{}".format( _uri, _link ),
                        'value': _value,
                        'weight': _weight,
                        'related_quests': None,
                        'extra': _extra
                    }
                )

        # CHEMS
        # the tables are sructured differently, so we'll need to parse each separately
        _rows = _tables[3].findAll( 'tr' )

        # loop the rows
        for row in _rows:

            # get our columns
            cols = row.findAll( 'td' )

            if cols:

                # get a link to the items details page
                _link = cols[1].find( 'a' ).attrs.get( "href" )

                # now we can strip everything out from it row, and just return it's textual content
                cols=[x.text.strip() for x in cols]
                _name = cols[1]
                _value = cols[6]
                _weight = cols[5]

                # add in the extra data
                _extra = []
                _extra.append( {
                    'effect' : cols[2], 
                    'food' : cols[3], 
                    'water' : cols[4], 
                    'addiction' : cols[7]
                } )

                _items.append(
                    {
                        'name': _name,
                        'image': None,
                        'link': "{}{}".format( _uri, _link ),
                        'value': _value,
                        'weight': _weight,
                        'related_quests': None,
                        'extra': _extra
                    }
                )

        # MISCELANEOUS
        # the tables are sructured differently, so we'll need to parse each separately
        _rows = _tables[4].findAll( 'tr' )

        # loop the rows
        for row in _rows:

            # get our columns
            cols = row.findAll( 'td' )

            if cols:

                # get a link to the items details page
                _link = cols[1].find( 'a' ).attrs.get( "href" )

                # now we can strip everything out from it row, and just return it's textual content
                cols=[x.text.strip() for x in cols]
                _name = cols[1]
                _value = cols[5]
                _weight = cols[4]

                # add in the extra data
                _extra = []
                _extra.append( {
                    'effect' : cols[2], 
                    'rads' : cols[3], 
                    'addiction' : cols[6]
                } )

                _items.append(
                    {
                        'name': _name,
                        'image': None,
                        'link': "{}{}".format( _uri, _link ),
                        'value': _value,
                        'weight': _weight,
                        'related_quests': None,
                        'extra': _extra
                    }
                )
        
        return _items