#! /usr/bin/env python3

from bs4 import BeautifulSoup

class KP_FO76_Plans:

    def ListData( self, _resp, _uri ):

        # all items`
        _items = []

        # fire up the soup to parse the content
        _soup = BeautifulSoup( _resp.text, 'lxml' )

        # get our datatables
        _tables = _soup.findAll( 'table', {'class': 'va-table'} )

        # valid tables on page, they are structure the same, so we can loop...
        for _table in _tables:

            # the tables are sructured differently, so we'll need to parse each separately
            _rows = _table.findAll( 'tr' )

            # loop the rows
            for row in _rows:

                # get our columns
                cols = row.findAll( 'td' )

                if cols:

                    # get a link to the items details page
                    _link = cols[0].find( 'a' ).attrs.get( "href" )

                    # process quests
                    _quests = cols[3].findAll( 'a' )
                    _quest_links = []
                    # if there is something here... get the urls in a list
                    if _quests:
                        for _quest in _quests:
                            _quest_links.append( {
                                'url' : "{}{}".format( _uri, _quest.attrs.get( "href" ) ),
                                'text' : _quest.getText( )
                            } )

                    # now we can strip everything out from it row, and just return it's textual content
                    cols=[x.text.strip() for x in cols]
                    _name = cols[0]
                    _value = cols[2]
                    _weight = cols[1]

                    _items.append(
                        {
                            'name': _name,
                            'image': None,
                            'link': "{}{}".format( _uri, _link ),
                            'value': _value,
                            'weight': _weight,
                            'related_quests': _quest_links,
                            'extra': None
                        }
                    )
        
        return _items
